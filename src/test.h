#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_MASTER_TEST_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_MASTER_TEST_H


#include "mem.h"
#include "mem_internals.h"

void tests_run_all(void* heap);

#endif
