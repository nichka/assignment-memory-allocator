#include <stdio.h>
#include "test.h"

#define SIZE 50
#define KILOSIZE 1050
#define MEGASIZE 10050


static void test_1(void* heap) {
    printf("Test 1\n");
    void *block = _malloc(SIZE);
    debug_heap(stdout, heap);
    if (!block) {
        printf("Test failed\n");
        return;
    }
    _free(block);
    printf("Test passed\n");
}

static void test_2(void* heap){
    printf("Test 2\n");
    void *block_1 = _malloc(SIZE);
    void *block_2 = _malloc(SIZE);
    debug_heap(stdout, heap);
    if (!block_1 || !block_2) {
        printf("Test failed\n");
        return;
    }
    _free(block_1);
    debug_heap(stdout, heap);
    _free(block_2);
    debug_heap(stdout, heap);
    printf("Test passed\n");
}

static void test_3(void* heap){
    printf("Test 3\n");
    void *block_1 = _malloc(SIZE);
    void *block_2 = _malloc(SIZE);
    void *block_3 =_malloc(SIZE);
    debug_heap(stdout, heap);
    if (!block_1 || !block_2 || !block_3) {
        printf("Test failed\n");
        return;
    }
    _free(block_1);
    debug_heap(stdout, heap);
    _free(block_2);
    debug_heap(stdout, heap);
    _free(block_3);
    debug_heap(stdout, heap);
    printf("Test passed\n");
}

static void test_4(void* heap) {
    printf("Test 4\n");
    void *block_1 =_malloc(SIZE);
    debug_heap(stdout, heap);
    void *block_2 = _malloc(KILOSIZE);
    debug_heap(stdout, heap);
    _free(block_1);
    _free(block_2);
    debug_heap(stdout, heap);
    printf("Test passed\n");
}

static void test_5(void* heap) {
    printf("Test 5\n");
    void *block =_malloc(SIZE);
    debug_heap(stdout, heap);
    struct block_header* block_header;
    block_header = block_get_header(block);
    void *test_block_header = block_header + 10000;
    mmap(test_block_header, 1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, 0, 0);
    _malloc(MEGASIZE);
    debug_heap(stdout, heap);
    _free(block);
    debug_heap(stdout, heap);
    printf("Test passed\n");
}

void tests_run_all(void* heap){
    test_1(heap);
    test_2(heap);
    test_3(heap);
    test_4(heap);
    test_5(heap);
}