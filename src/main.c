#include "test.h"


int main() {
    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    tests_run_all(heap);
    munmap(heap,REGION_MIN_SIZE);
    return 0;
}
